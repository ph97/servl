package com.isoft.intern.Entity;

/**
 * @author Petar Hristov
 * Entity class
 */
public class AuthorBookJoined {
    private String authorFirstName;
    private String authorLastName;
    private String authorBirthDate;
    private String bookTitle;
    private String bookYear;

    public AuthorBookJoined(String authorFirstName, String authorLastName, String authorBirthDate, String bookTitle, String bookYear) {
        this.authorFirstName = authorFirstName;
        this.authorLastName = authorLastName;
        this.authorBirthDate = authorBirthDate;
        this.bookTitle = bookTitle;
        this.bookYear = bookYear;
    }

    public String getAuthorFirstName() {
        return authorFirstName;
    }

    public void setAuthorFirstName(String authorFirstName) {
        this.authorFirstName = authorFirstName;
    }

    public String getAuthorLastName() {
        return authorLastName;
    }

    public void setAuthorLastName(String authorLastName) {
        this.authorLastName = authorLastName;
    }

    public String getAuthorBirthDate() {
        return authorBirthDate;
    }

    public void setAuthorBirthDate(String authorBirthDate) {
        this.authorBirthDate = authorBirthDate;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public String getBookYear() {
        return bookYear;
    }

    public void setBookYear(String bookYear) {
        this.bookYear = bookYear;
    }
}
