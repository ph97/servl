package com.isoft.intern.Entity;

/**
 * @author Petar Hristov
 * Entity class
 */
public class AuthorBook {
    Integer authorId;
    Integer bookId;

    public AuthorBook(){}

    public AuthorBook(Integer authorId, Integer bookId) {
        this.authorId = authorId;
        this.bookId = bookId;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }
}
