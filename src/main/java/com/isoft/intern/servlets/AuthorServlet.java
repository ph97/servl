package com.isoft.intern.servlets;

import com.google.gson.JsonParseException;
import com.isoft.intern.Entity.Author;
import com.isoft.intern.services.AuthorService;
import com.isoft.intern.util.RequestReader;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

/**
 * @author Petar Hristov
 * Servlet which handles the request by its type.
 */

public class AuthorServlet extends HttpServlet {
    private AuthorService authorService;

    @Override
    public void init() throws ServletException {
        this.authorService = new AuthorService();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try{
            Author author = RequestReader.readRequest(req.getReader(), Author.class);
            authorService.insert(author);
        }catch (NullPointerException ex){
            resp.getWriter().print("Please provide a json payload body with the inforamation you wish to insert.");}
        catch (JsonParseException ex){
            resp.getWriter().print("The json payload body syntax is invalid.");}
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("id") != null)
            authorService.delete(Integer.parseInt(req.getParameter("id")));
        else
            resp.getWriter().print("Please provide an id parameter with the id you wish to delete.");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try
        {
            if(req.getParameter("id") != null) {
                Author author = RequestReader.readRequest(req.getReader(), Author.class);
                authorService.update(author, Integer.parseInt(req.getParameter("id")));
            }
            else
                resp.getWriter().print("Please provide an id , you wish to update with and a valid json");
        }
        catch (NullPointerException ex){
            resp.getWriter().print("Please provide a json payload body with the inforamation you wish to insert.");}
        catch (JsonParseException ex){
            resp.getWriter().print("The json payload body syntax is invalid.");}
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        resp.setContentType("application/json");
        try {
            if (req.getParameter("id") != null) {
                out.print(RequestReader.writeJson(authorService.findById(Integer.parseInt(req.getParameter("id")))));
            }
            else {
                out.print(RequestReader.writeJson(authorService.findAll()));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
