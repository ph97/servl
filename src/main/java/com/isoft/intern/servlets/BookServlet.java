package com.isoft.intern.servlets;

import com.google.gson.JsonParseException;
import com.isoft.intern.Entity.Book;
import com.isoft.intern.services.BookService;
import com.isoft.intern.util.RequestReader;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * @author Petar Hristov
 * Servlet which handles the request by its type.
 */

public class BookServlet extends HttpServlet {
    private BookService bookService;

    @Override
    public void init() throws ServletException {
        this.bookService = new BookService();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try{
            Book book = RequestReader.readRequest(req.getReader(), Book.class);
            bookService.insert(book);
        }catch (NullPointerException ex){
            resp.getWriter().print("Please provide a json payload body with the inforamation you wish to insert.");}
        catch (JsonParseException ex){
            resp.getWriter().print("The json payload body syntax is invalid.");}
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("id") != null)
            bookService.delete(Integer.parseInt(req.getParameter("id")));
        else
            resp.getWriter().print("Please provide an id parameter with the id you wish to delete.");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try
        {
            if(req.getParameter("id") != null) {
                Book book = RequestReader.readRequest(req.getReader(), Book.class);
                bookService.update(book, Integer.parseInt(req.getParameter("id")));
            }
            else
                resp.getWriter().print("Please provide an id , you wish to update with and a valid json");
        }
        catch (NullPointerException ex){
            resp.getWriter().print("Please provide a json payload body with the inforamation you wish to insert.");}
        catch (JsonParseException ex){
            resp.getWriter().print("The json payload body syntax is invalid.");}
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        try {
            if (req.getParameter("id") != null) {
                resp.getWriter().print(RequestReader.writeJson(bookService.findById(Integer.parseInt(req.getParameter("id")))));
            }
            else {
                resp.getWriter().print(RequestReader.writeJson(bookService.findAll()));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
