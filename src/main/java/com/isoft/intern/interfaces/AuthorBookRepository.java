package com.isoft.intern.interfaces;

import com.isoft.intern.Entity.AuthorBook;
import com.isoft.intern.Entity.AuthorBookJoined;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Petar Hristov
 * interface made with the purpose of having specific operations
 * ,inheritates the main CRUD operations
 */
public interface AuthorBookRepository extends CRUDRepo<AuthorBook,Integer> {
    List<AuthorBookJoined> findAllJoined() throws SQLException;
}
