package com.isoft.intern.interfaces;

import com.isoft.intern.Entity.Book;

public interface BookRepository extends CRUDRepo<Book,Integer> {
    
}
