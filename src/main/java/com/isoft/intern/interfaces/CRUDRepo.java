package com.isoft.intern.interfaces;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Petar Hristov
 * Interface containing the main operations
 */

public interface CRUDRepo<E,ID> {


    void insert(E entity);
    void delete(ID id);
    void update(E entity,ID id);

    List<E> findAll() throws SQLException;
    E findById(ID id) throws SQLException;
}
