package com.isoft.intern.interfaces;

import com.isoft.intern.Entity.Author;

public interface AuthorRepository extends CRUDRepo<Author,Integer>{

}
