package com.isoft.intern;
import com.isoft.intern.database.DatabaseCommunication;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @author Petar Hristov
 * Init class executed when the server is started
 */

public class InitServer implements ServletContextListener{
    private DatabaseCommunication dbc;
    private String dbUrl="jdbc:postgresql://localhost:5432/isoftdb";
    private String user="admin";
    private String password="admin";

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent)
    {
        dbc = new DatabaseCommunication(dbUrl,user,password);
        dbc.initDriver();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent)
    {
        dbc.close();
    }
}
