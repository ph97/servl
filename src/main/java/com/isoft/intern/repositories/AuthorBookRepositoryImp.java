package com.isoft.intern.repositories;


import com.isoft.intern.Entity.AuthorBook;
import com.isoft.intern.Entity.AuthorBookJoined;
import com.isoft.intern.database.DatabaseCommunication;
import com.isoft.intern.interfaces.AuthorBookRepository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Petar Hristov
 */

public class AuthorBookRepositoryImp implements AuthorBookRepository {
    DatabaseCommunication dbc=DatabaseCommunication.getInstance();

    public AuthorBookRepositoryImp() { }

    @Override
    public void insert(AuthorBook authorBook) {
        String query=String.format("INSERT INTO author_book (author_id, book_id)" +
                "VALUES (%d,%d);",authorBook.getAuthorId(),authorBook.getBookId());
        dbc.executeQuery(query);
    }

    @Override
    public void delete(Integer id) {
        String query=String.format("DELETE FROM author_book " +
                "WHERE ab_id = %d",id);
        dbc.executeQuery(query);

    }

    @Override
    public void update(AuthorBook authorBook,Integer id) {
        String query=String.format("UPDATE author_book " +
                "SET author_id = %d, book_id=%d "+
                "WHERE ab_id = %d",authorBook.getAuthorId(),authorBook.getBookId(),id);
        dbc.executeQuery(query);
    }

    @Override
    public List<AuthorBook> findAll() throws SQLException {
        String query="SELECT * FROM author_book";

        List<AuthorBook> authorBooks = new ArrayList<>();

        PreparedStatement st = dbc.getConnection().prepareStatement(query);
        ResultSet rs = st.executeQuery();
        while (rs.next())
        {
            authorBooks.add(new AuthorBook(Integer.parseInt(rs.getString("author_id")),Integer.parseInt(rs.getString("book_id"))));
        }
        rs.close();
        st.close();
        return authorBooks;
    }

    @Override
    public List<AuthorBookJoined> findAllJoined() throws SQLException {
        String query="SELECT author_first_name,author_last_name,author_birth_date,book_title,book_year "+
                "FROM author_book ab"+
                "INNER JOIN author a "+
                "ON ab.author_id = a.author_id, "+
                "INNER JOIN book b "+
                "ON ab.book_id = b.book_id";

        List<AuthorBookJoined> authorBooksJoined = new ArrayList<>();

        PreparedStatement st = dbc.getConnection().prepareStatement(query);
        ResultSet rs = st.executeQuery();
        while (rs.next())
        {
            authorBooksJoined.add(new AuthorBookJoined(rs.getString("author_first_name"),rs.getString("author_last_name"),rs.getString("author_birth_date"),rs.getString("book_title"),rs.getString("book_year")));
        }
        rs.close();
        st.close();
        return authorBooksJoined;
    }

    @Override
    public AuthorBook findById(Integer id) throws SQLException {
        String query=String.format("SELECT * FROM author_book "+
                "WHERE ab_id=%d",id);

        AuthorBook authorBook = new AuthorBook();
        PreparedStatement st = dbc.getConnection().prepareStatement(query);
        ResultSet rs = st.executeQuery();
        while(rs.next())
        {
            authorBook.setAuthorId(Integer.parseInt(rs.getString("author_id")));
            authorBook.setBookId(Integer.parseInt(rs.getString("book_id")));
        }
        return authorBook;
    }
}
