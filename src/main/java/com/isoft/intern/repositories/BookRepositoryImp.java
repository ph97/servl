package com.isoft.intern.repositories;

import com.isoft.intern.Entity.Book;
import com.isoft.intern.database.DatabaseCommunication;
import com.isoft.intern.interfaces.BookRepository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookRepositoryImp implements BookRepository {
    DatabaseCommunication dbc=DatabaseCommunication.getInstance();

    public BookRepositoryImp() { }

    @Override
    public void insert(Book book) {
        String query=String.format("INSERT INTO book (book_title, book_year)" +
                "VALUES ('%s','%s');",book.getBookTitle(),book.getBookYear());
        dbc.executeQuery(query);
    }

    @Override
    public void delete(Integer id) {
        String query=String.format("DELETE FROM book " +
                "WHERE book_id = %d",id);
        dbc.executeQuery(query);
    }

    @Override
    public void update(Book book, Integer id) {
        String query=String.format("UPDATE book " +
                "SET book_title = '%s' , book_year= '%s' "+
                "WHERE author_id = %d",book.getBookTitle(),book.getBookYear(),id);
        dbc.executeQuery(query);
    }

    @Override
    public List<Book> findAll() throws SQLException {
        String query="SELECT * FROM book";
        List<Book> books = new ArrayList<>();

        PreparedStatement st = dbc.getConnection().prepareStatement(query);
        ResultSet rs = st.executeQuery();
        while (rs.next())
        {
            books.add(new Book(rs.getString("book_title"),rs.getString("book_year")));
        }
        rs.close();
        st.close();
        return books;
    }

    @Override
    public Book findById(Integer id) throws SQLException {
        String query=String.format("SELECT * FROM book "+
                "WHERE author_id=%d",id);

        Book book = new Book();
        PreparedStatement st = dbc.getConnection().prepareStatement(query);
        ResultSet rs = st.executeQuery();
        while(rs.next())
        {
            book.setBookTitle(rs.getString("book_title"));
            book.setBookYear(rs.getString("book_year"));
        }
        return book;
    }
}
