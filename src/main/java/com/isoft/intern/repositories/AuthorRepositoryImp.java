package com.isoft.intern.repositories;

import com.isoft.intern.Entity.Author;
import com.isoft.intern.database.DatabaseCommunication;
import com.isoft.intern.interfaces.AuthorRepository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class AuthorRepositoryImp implements AuthorRepository {
    DatabaseCommunication dbc=DatabaseCommunication.getInstance();

    public AuthorRepositoryImp() { }

    @Override
    public void insert(Author author) {
        String query=String.format("INSERT INTO author (author_first_name, author_last_name, author_birth_date)" +
                                   "VALUES ('%s','%s','%s');",author.getFirstName(),author.getLastName(),author.getBirthDate());
        dbc.executeQuery(query);
    }

    @Override
    public void delete(Integer id) {
        String query=String.format("DELETE FROM author " +
                                   "WHERE author_id = %d",id);
        dbc.executeQuery(query);

    }

    @Override
    public void update(Author author,Integer id) {
        String query=String.format("UPDATE author " +
                "SET author_first_name = '%s' , author_last_name='%s' , author_birth_date='%s' "+
                "WHERE author_id = %d",author.getFirstName(),author.getLastName(),author.getBirthDate(),id);
        dbc.executeQuery(query);
    }

    @Override
    public List<Author> findAll() throws SQLException {
        String query="SELECT * FROM author";
        List<Author> authors = new ArrayList<>();

        PreparedStatement st = dbc.getConnection().prepareStatement(query);
        ResultSet rs = st.executeQuery();
        while (rs.next())
        {
            authors.add(new Author(rs.getString("author_first_name"),rs.getString("author_last_name"),rs.getString("author_birth_date")));
        }
        rs.close();
        st.close();
        return authors;
    }

    @Override
    public Author findById(Integer id) throws SQLException {
        String query=String.format("SELECT * FROM author "+
            "WHERE author_id=%d",id);

        Author author = new Author();
        PreparedStatement st = dbc.getConnection().prepareStatement(query);
        ResultSet rs = st.executeQuery();
        while(rs.next())
        {
            author.setFirstName(rs.getString("author_first_name"));
            author.setLastName(rs.getString("author_last_name"));
            author.setBirthDate(rs.getString("author_birth_date"));
        }
        return author;
    }
}
