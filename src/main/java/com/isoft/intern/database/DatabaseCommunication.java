package com.isoft.intern.database;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Petar Hristov
 * This class handles the communication with the postgre database
 */

public class DatabaseCommunication {
    private Connection connection;
    private String url;
    private String user;
    private String password;
    private static DatabaseCommunication instance;
    private static final Logger LOGGER = Logger.getLogger(DatabaseCommunication.class.getName());

    private DatabaseCommunication(){}

    public DatabaseCommunication(String url, String user, String password)
    {
        this.url = url;
        this.user = user;
        this.password = password;
        instance=this;
    }

    /**
     * Checks if it is already instantiated
     * @return the instance
     */
    public static DatabaseCommunication getInstance()
    {
        if(instance == null)
        {
           LOGGER.log(Level.WARNING,"DBC null instance");
        }
        return instance;
    }

    /**
     * Method which initializes the postgre driver
     */
    public void initDriver()
    {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method which connects to the DB
     */
    public void connect()
    {
        Connection conn = null;
        try
        {
            conn = DriverManager.getConnection(url, user, password);
            LOGGER.log(Level.WARNING,"Connected to the PostgreSQL server successfully.");
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,e.getMessage());
            return;
        }
       this.connection=conn;
    }

    public Connection getConnection() {
        return connection;
    }

    public void executeQuery(String sqlQuery)
    {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sqlQuery);
            ps.executeUpdate();
            LOGGER.log(Level.WARNING,"Query executed successfully.\"+\"Query:"+sqlQuery);
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING,"There was an error executing the query.Query:"+sqlQuery);
        }
    }

    public void close()
    {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
