package com.isoft.intern.util;


import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;

/**
 * @author Petar Hristov
 * Reads the request body(which is json) and converts it to java object
 */

public class RequestReader {
    private static Gson gson = new Gson();

    public static <T> T readRequest(BufferedReader reader, Class<T> type) throws IOException {
        StringBuilder json = new StringBuilder();
        String line;
        while((line=reader.readLine()) != null)
        {
            json.append(line);
        }

        return gson.fromJson(json.toString(), type);
    }

    public static String writeJson(Object obj)
    {
        String jsonInString = gson.toJson(obj);
        return jsonInString;
    }



}
