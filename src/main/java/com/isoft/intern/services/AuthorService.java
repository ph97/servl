package com.isoft.intern.services;


import com.isoft.intern.Entity.Author;
import com.isoft.intern.interfaces.AuthorRepository;
import com.isoft.intern.repositories.AuthorRepositoryImp;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Petar Hristov
 * Service class which redirects the operation requested to the class where it is implemented.
 */

public class AuthorService {

    private AuthorRepository repository;

    public AuthorService()
    {
        this.repository= new AuthorRepositoryImp();
    }

    public void insert(Author author)
    {
        this.repository.insert(author);
    }

    public void delete(Integer id)
    {
        this.repository.delete(id);
    }

    public void update(Author author,Integer id)
    {
        this.repository.update(author,id);
    }

    public List<Author> findAll() throws SQLException {return this.repository.findAll(); }

    public Author findById(Integer id) throws SQLException {return this.repository.findById(id);}

}
