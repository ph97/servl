package com.isoft.intern.services;

import com.isoft.intern.Entity.Book;
import com.isoft.intern.interfaces.BookRepository;
import com.isoft.intern.repositories.BookRepositoryImp;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Petar Hristov
 * Service class which redirects the operation requested to the class where it is implemented.
 */

public class BookService {
    private BookRepository repository;

    public BookService()
    {
        this.repository= new BookRepositoryImp();
    }

    public void insert(Book book)
    {
        this.repository.insert(book);
    }

    public void delete(Integer id)
    {
        this.repository.delete(id);
    }

    public void update(Book book,Integer id)
    {
        this.repository.update(book,id);
    }

    public List<Book> findAll() throws SQLException {return this.repository.findAll(); }

    public Book findById(Integer id) throws SQLException {return this.repository.findById(id);}
}
