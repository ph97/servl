package com.isoft.intern.services;


import com.isoft.intern.Entity.AuthorBook;
import com.isoft.intern.Entity.AuthorBookJoined;
import com.isoft.intern.interfaces.AuthorBookRepository;
import com.isoft.intern.repositories.AuthorBookRepositoryImp;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Petar Hristov
 * Service class which redirects the operation requested to the class where it is implemented.
 */

public class AuthorBookService {

    private AuthorBookRepository repository;

    public AuthorBookService() { this.repository= new AuthorBookRepositoryImp(); }

    public void insert(AuthorBook authorBook) { this.repository.insert(authorBook); }

    public void delete(Integer id) { this.repository.delete(id); }

    public void update(AuthorBook authorBook,Integer id) { this.repository.update(authorBook,id); }

    public List<AuthorBook> findAll() throws SQLException {return this.repository.findAll(); }

    public List<AuthorBookJoined> findAllJoined() throws SQLException {return this.repository.findAllJoined();}

    public AuthorBook findById(Integer id) throws SQLException {return this.repository.findById(id);}
}
